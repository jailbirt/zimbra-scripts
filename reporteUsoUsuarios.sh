#!/bin/bash
#Adapt This Values
notify=admin@ign.gob.ar
notifyCC=jailbirt@ign.gob.ar,jgrilli@ign.gob.ar,ebordon@ign.gob.ar,fpichilli@ign.gob.ar

#All defined domains running on this server
domains="ign.gob.ar
idera.gob.ar
sig.gov.ar	
igm.gov.ar
ideargentina.gob.ar
prosiga.gob.ar
"
#Email subjects for each type of report
subjectUso="Informe de Uso por cuenta"     
subjectAlLimite="Informe de Cuentas por bloquearse, quedan 50MB"
subjectExcedido="Informe de Cuentas bloqueadas"
#END

#Email subjects for warning users
subjectUsuarioAlLimite="=CUIDADO= QUEDA POCO ESPACIO EN SU CASILLA DE CORREO"
subjectUsuarioExcedido="=ATENCION= CASILLA DE CORREO LLENA NO RECIBE MAS MAILS"
mensajeUsuarioExcedido="Su casilla ha sido bloqueada, por favor elimine emails viejos o comuniquese con el dto. de sistemas. 
                        Gracias."
mensajeUsuarioAlLimite="Quedan solo 50MB disponibles, al alcanzar el limite ud. no recibira mas correos, por favor elimine emails viejos o comuniquese con el dto. de sistemas. 
gracias." 
 
#

epicFace="( ͡° ͜ʖ ͡°)"

WHO=`whoami`
if [ $WHO != "zimbra" ];then
   echo "Execute this script as user zimbra (\"su – zimbra\")"
   exit 
fi
mail=/usr/bin/mail

reporteUso=/tmp/reporteUsoEnMega.log
reporteExcedido=/tmp/reporteExcesoEnBytes.log
reporteAlLimite=/tmp/reporteBloqueados.log

#blank Reports.
echo " ">$reporteUso
echo " ">$reporteExcedido
echo " ">$reporteAlLimite

#All data to be analized.
dataCruda=/tmp/dataCruda.txt
zmprov gqu localhost > $dataCruda
#Reporte de Uso

#html Stuff
header="<table style='width:300px;$style'>
        <tr style='border:1px solid black;'><td>$domain</td><td></td><td></td></tr> 
        <tr style='border:1px solid black;'><td>Usuario</td><td> EspacioUtilizado</td><td> LimiteQuota</td></tr>" 
style="border:1px solid black; border-collapse:collapse;"
footer="</table> 
  <br></br>
  <br></br>"

for domain in $domains
do
  echo $header >>$reporteUso
  cat $dataCruda|grep $domain |awk {'print "<tr><td> "$1"</td><td> "$3/1024/1024"MB</td> <td>"$2/1024/1024"MB</td> </tr>"'} >>$reporteUso 
  echo $footer >>$reporteUso
done

#Reporte a 100MB de bloquearse.
for domain in $domains
do
  echo $header >>$reporteAlLimite
  #Reporte de Pasados de Quota, este disparà un mail aparte, creo que nadie lee los mails diarios ;)
  cat $dataCruda|grep $domain | awk '{if ($3+52428800 > $2 && $2 != 0) print "<tr><td> "$1"</td><td> "$3/1024/1024"MB</td> <td>"$2/1024/1024"MB</td> </tr>"}'>> $reporteAlLimite
  echo $footer >>$reporteAlLimite
done

for domain in $domains
do
  echo $header >>$reporteExcedido
  #Reporte de Pasados de Quota, este disparà un mail aparte, creo que nadie lee los mails diarios ;)
  cat $dataCruda|grep $domain | awk '{if ($3 > $2 && $2 != 0) print "<tr><td> "$1"</td><td> "$3/1024/1024"MB</td> <td>"$2/1024/1024"MB</td> </tr>"}'>> $reporteExcedido
  echo $footer >>$reporteExcedido
done

echo -e "developed by UTI $epicFace" >> $reporteUso
echo -e "developed by UTI $epicFace" >> $reporteAlLimite
echo -e "developed by UTI $epicFace" >> $reporteExcedido

echo "Enviando Reportes"
#envia reportes
cat $reporteUso     | $mail -a 'Content-Type: text/html' -s "$subjectUso" -t $notifyCC $notify
cat $reporteAlLimite| $mail -a 'Content-Type: text/html' -s "$subjectAlLimite" -t $notifyCC $notify
cat $reporteExcedido| $mail -a 'Content-Type: text/html' -s "$subjectExcedido" -t $notifyCC $notify

echo "Enviando Aviso a los usuarios"
#envia aviso a los usuarios
for usuarioExcedido in $(cat $dataCruda| awk '{if ($3 > $2 && $2 != 0) print $1}')
do
  echo "excedido $usuarioExcedido <-"
  echo $mensajeUsuarioExcedido | $mail -s "$subjectUsuarioExcedido" -t $notify $usuarioExcedido
done
for usuarioAlLimite in $(cat $dataCruda| awk '{if ($3+52428800 > $2 && $2 != 0) print $1}')
do
  echo "al limite $usuarioAlLimite <-"
  echo $mensajeUsuarioAlLimite | $mail -s "$subjectUsuarioAlLimite" -t $notify $usuarioAlLimite
done

