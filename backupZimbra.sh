#!/bin/bash

# Zimbra Backup Script
# Requires that you have ssh-keys: https://help.ubuntu.com/community/SSHHowto#Public%20key%20authentication
# This script is intended to run from the crontab as root
# Date outputs and su vs sudo corrections by other contributors, thanks, sorry I don't have names to attribute!
# Free to use and free of any warranty!  Daniel W. Martin, 5 Dec 2008
## Adapted for rsync over ssh instead of ncftp by Ace Suares, 24 April 2009 (Ubuntu 6.06 LTS)

#Adaptado por jailbirt para el ente, no tiene nada local es todo en el NAS
#Tambien tiene rotacion

# the destination directory for local backups
DESTLOCAL=/media/backupLocal
DESTREMOTO=/media/backupShare

# Outputs the time the backup started, for log/tracking purposes
echo Time backup started = $(date +%T) > /tmp/zbackup$(date +%u).log 2>&1
before="$(date +%s)"

# Live sync before stopping Zimbra to minimize sync time with the services down
# Comment out the following line if you want to try single cold-sync only

#exclude del sparce file, esta mal copiarlo a lo bruto.
rsync -avHK --delete --exclude "data.mdb" --exclude "data.mdb.old" /opt/zimbra/ $DESTLOCAL/zimbra >> /tmp/zbackup$(date +%u)_rsync.log 2>&1

# which is the same as: /opt/zimbra /backup 
# Including --delete option gets rid of files in the dest folder that don't exist at the src 
# this prevents logfile/extraneous bloat from building up overtime.
# the backupdir will hold all files that changed or where deleted during the previous backup

# Now we need to shut down Zimbra to rsync any files that were/are locked
# whilst backing up when the server was up and running.
before2="$(date +%s)"

# Stop Zimbra Services
/etc/init.d/zimbra stop >> /tmp/zbackup$(date +%u).log 2>&1

#su - zimbra -c"/opt/zimbra/bin/zmcontrol stop"
#sleep 15
# Kill any orphaned Zimbra processes
#kill -9 `ps -u zimbra -o "pid="`
pkill -9 -u zimbra >> /tmp/zbackup$(date +%u).log 2>&1


# Only enable the following command if you need all Zimbra user owned
# processes to be killed before syncing
# ps auxww | awk '{print $1" "$2}' | grep zimbra | kill -9 `awk '{print $2}'`

# Sync to backup directory
#Denuevo, obvio los sparse files, los copio manualmente con cp -S una vez que ldap esta frenado.
rsync -avHK --delete  --exclude "data.mdb" --exclude "data.mdb.old" /opt/zimbra/ $DESTLOCAL/zimbra  >> /tmp/zbackup$(date +%u)_rsync.log 2>&1

#Para que no escanee los 80gb, y trate el sparse file como debe ser
/opt/zimbra/rsync/bin/rsync -S /opt/zimbra/data/ldap/mdb/db/data.mdb $DESTLOCAL/zimbra/data/ldap/mdb/db/data.mdb >> /tmp/zbackup$(date +%u)_rsync.log 2>&1

# Restart Zimbra Services
#su - zimbra -c "/opt/zimbra/bin/zmcontrol start"
/etc/init.d/zimbra start   >> /tmp/zbackup$(date +%u).log 2>&1


# Calculates and outputs amount of time the server was down for
after="$(date +%s)"
elapsed="$(expr $after - $before2)"
hours=$(($elapsed / 3600))
elapsed=$(($elapsed - $hours * 3600))
minutes=$(($elapsed / 60))
seconds=$(($elapsed - $minutes * 60))
echo SERVER WAS DOWN FOR: "$hours hours $minutes minutes $seconds seconds"  >> /tmp/zbackup$(date +%u).log 2>&1

# Create a txt file in the backup directory that'll contains the current Zimbra
# server version. Handy for knowing what version of Zimbra a backup can be restored to.
# su - zimbra -c "zmcontrol -v > $DESTLOCAL/zimbra/conf/zimbra_version.txt"
# or examine your /opt/zimbra/.install_history

# Display Zimbra services status
echo Displaying Zimbra services status... >> /tmp/zbackup$(date +%u).log 2>&1
su - zimbra -c "/opt/zimbra/bin/zmcontrol status"  >> /tmp/zbackup$(date +%u).log 2>&1

# /etc/init.d/zimbra status # seems not to work
# backup the backup dir (but not the backups of the backups) to remote
#rsync -essh -avHK --delete-during $DESTLOCAL/zimbra $DESTREMOTE


# Outputs the time the backup finished
echo Time backup finished = $(date +%T)  >> /tmp/zbackup$(date +%u).log 2>&1

# Calculates and outputs total time taken
after="$(date +%s)"
elapsed="$(expr $after - $before)"
hours=$(($elapsed / 3600))
elapsed=$(($elapsed - $hours * 3600))
minutes=$(($elapsed / 60))
seconds=$(($elapsed - $minutes * 60))
echo Time taken: "$hours hours $minutes minutes $seconds seconds" >> /tmp/zbackup$(date +%u).log 2>&1

mail=/usr/bin/mail
notify=admin@ign.gob.ar
notifyCC=jailbirt@ign.gob.ar,fpichilli@ign.gob.ar,jgrilli@ign.gob.ar,ebordon@ign.gob.ar
subject=" Corrió el backup del zimbra el día $(date) " 

cat /tmp/zbackup$(date +%u).log| $mail -s "$subject" -t $notifyCC $notify

cd $DESTLOCAL
#Autorotacion 1 al 7, mirar las fechas ante la duda...
tar -cvzf $DESTLOCAL/zimbra_$(date +%u).tar.gz zimbra 

#Copio a FS Remoto.
smbmount //172.20.200.7/U$ $DESTREMOTO/ -o username=administrador,password=root**igndc0,rw
ls -l /media/ |grep backupShare|grep ^d\?
if [ $? == 0 ];then
  exit 
fi
cp zimbra_$(date +%u).tar.gz $DESTREMOTO/
# end
#Llamo al procurador de Quotas.
sudo su - zimbra -c "/usr/local/bin/reporteUsoUsuarios.sh"
